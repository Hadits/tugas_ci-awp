<div class="container">
  <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto ">
    <div class="card-header">Register</div>
    <div class="card-body">
      <form class="user" method="post" action="<?= base_url('ctrl/register') ?>">
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-12">
              <div class="form-label-group">
                <input type="text" id="name" name="name" class="form-control" placeholder="Fullname" autofocus="autofocus" value="<?= set_value('Name'); ?>">
                <label for="inputEmail">Fullname</label>
                <?= form_error('name', '<small class="text-danger">', '</small>'); ?>

              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="form-label-group">
            <input type="text" id="Email" name="email" class="form-control">
            <label for="inputEmail">Email address</label>
            <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
          </div>
        </div>
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="password" id="Password1" name="password1" class="form-control" placeholder="Password">
                <label for="inputPassword">Password</label>
                <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="password" id="password2" name="password2" class="form-control" placeholder="Confirm password">
                <label for="confirmPassword">Confirm Password</label>
              </div>
            </div>
          </div>
        </div>
        <button class="btn btn-primary btn-block">Register</button>
      </form>
      <div class="text-center">
        <a class="d-block small mt-3" href="<?= base_url('ctrl') ?>">Login</a>
      </div>
    </div>
  </div>
</div>